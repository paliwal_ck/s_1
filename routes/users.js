var express = require('express');
var router = express.Router();

/* GET users detail. */
router.get('/:id', testMiddleWare, function (req, res, next) {
  res.json(200, {
    id: 1,
    name: 'Chandra Kant Paliwal'
  });
});

module.exports = router;

// In case we need to create Auth implementation then we can test authorization token through it and validate it from using passport and what ever our specific algo.
function testMiddleWare(req, res, next) {
  if (req.params.id === '1') {
    next();
  } else {
    var err = new Error();
    err.status = 422;
    err.message = 'Invalid access';
    next(err);
  }
}




// through the middleware we will achive ACL but it's not a too small JOB
// I have use whole ACL concept in my current project and ealier projects also
